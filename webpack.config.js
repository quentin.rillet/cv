var Encore = require('@symfony/webpack-encore');

Encore
// the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .addEntry('js/app', './assets/ts/app.ts')
    .addEntry('js/cv', './assets/ts/cv.ts')
    .addEntry('js/recommendation', './assets/ts/recommendation.ts')
    //.addEntry('js/demo3D', './assets/ts/demo3D.ts')
    .enableVueLoader()
    .addLoader({
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
            appendTsSuffixTo: [/\.vue$/]
        }
    })
    .enableForkedTypeScriptTypesChecking()

    .addStyleEntry('css/app', './assets/scss/app.scss')
    .enableSassLoader()
    .enablePostCssLoader()
    //.autoProvidejQuery()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Materialize: 'materialize-css/dist/js/materialize.min.js'
    })
;

module.exports = Encore.getWebpackConfig();
