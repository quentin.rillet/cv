import * as BABYLON from 'babylonjs';


export class Game {
    private _canvas: HTMLCanvasElement;
    private _engine: BABYLON.Engine;
    private _scene: BABYLON.Scene;
    private _camera: BABYLON.FreeCamera;
    private _light: BABYLON.Light;

    constructor(canvas: HTMLCanvasElement) {
        this._canvas = canvas
        this._engine = new BABYLON.Engine(this._canvas, false);
        this._engine.loadingUIText = "Loading";
        this._createScene();



        window.addEventListener("resize", () => {
            this._engine.resize();
        });
    };

    private _createScene() {
        this._scene = new BABYLON.Scene(this._engine);
        this._scene.clearColor = BABYLON.Color3.Blue().toColor4();
        this._scene.debugLayer.show();

        let skybox = BABYLON.MeshBuilder.CreateBox("skyBox", { size: 100 }, this._scene);

        let skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this._scene);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(
            "3d/skybox/skybox",
            this._scene
        );
        skyboxMaterial.reflectionTexture.coordinatesMode =
            BABYLON.Texture.SKYBOX_MODE;
        skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        skybox.material = skyboxMaterial;

        this._engine.resize();

        let light = new BABYLON.HemisphericLight(
            "light",
            new BABYLON.Vector3(-1, 1, 0),
            this._scene
        );
        light.diffuse = new BABYLON.Color3(1, 0, 0);
        let camera = new BABYLON.ArcRotateCamera(
            "Camera",
            -Math.PI / 2,
            Math.PI / 2, 5,
            BABYLON.Vector3.Zero(),
            this._scene
        );
        camera.setPosition(new BABYLON.Vector3(0, 10, -30));
        camera.attachControl(this._canvas, true);
        camera.useAutoRotationBehavior = true;
        
        let assetsManager = new BABYLON.AssetsManager(this._scene);
        assetsManager.addMeshTask("house", "", "./3d/", "house.babylon");
        assetsManager.onFinish = tasks => {
            this._engine.runRenderLoop(() => {
                this._scene.render();
            });

        };
        assetsManager.load();
    }

    public fullScreen() {
        this._engine.switchFullscreen(true);
    }
}