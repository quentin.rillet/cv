import Vue from 'vue'
import App from '../templates/Homepage/App.vue'
import VueI18n from 'vue-i18n'
import Vuelidate from 'vuelidate'
import axios from 'axios'
// import ScrollReveal from 'scrollreveal'
import fr from '../templates/Homepage/locales/fr'
import en from '../templates/Homepage/locales/en'

interface Window {
    sr: any;
    jQuery: any;
}

require('materialize-css')

// let $ = Window.jQuery = require('jquery');

axios.defaults.baseURL = '/api';
Vue.use(VueI18n);
Vue.use(Vuelidate);

let messages = {
    fr,
    en
};

const i18n = new VueI18n({
    locale: 'fr', // set locale
    messages, // set locale messages
});

new Vue({
    i18n,
    el: '#app',
    //router,
    template: '<App/>',
    components: {App}
});


/* configuration reveal plugin */
// window.sr = ScrollReveal();
// let baseDuration = 900;
// window.sr.reveal('.reveal', {
//     duration: baseDuration
// });
// window.sr.reveal('.reveal-1', {
//     duration: baseDuration + 300
// });
// window.sr.reveal('.reveal-2', {
//     duration: baseDuration + 600
// });
// window.sr.reveal('.reveal-3', {
//     duration: baseDuration + 900
// });
