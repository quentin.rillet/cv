let urlRecommendation: string = 'uploads/documents/recommendation-Quentin-Rillet.pdf';

interface Window {
    PDFJS: any;
}

window.PDFJS.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
// Asynchronous download of PDF
let loadingTaskRecommendation = window.PDFJS.getDocument(urlRecommendation);

loadingTaskRecommendation.promise.then((pdf: any) => {
    console.log('PDF loaded');
    // Fetch the first page
    let pageNumber = 1;
    pdf.getPage(pageNumber).then((page: any) => {
        console.log('Page loaded');
        let scale = 1;
        let viewport = page.getViewport(scale);
        // Prepare canvas using PDF page dimensions
        let canvas = <HTMLCanvasElement>document.getElementById('the-canvas');
        let context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        // Render PDF page into canvas context
        let renderContext = {
            canvasContext: context,
            viewport: viewport
        };
        let renderTask = page.render(renderContext);
        renderTask.then(() => {
            console.log('Page rendered')
        })
    })
}, (reason: any) => {
    console.error(reason)
});
