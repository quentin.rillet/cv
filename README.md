
# Pour le déploiement

https://deployer.org

``` composer log
composer install
composer global require deployer/deployer
```

### Assets

``` javascript
yarn 
```

### Fixtures

Créer un utilisateur 'admin' et mot de passe 'admin' par défaut

```
php bin/console doctrine:fixtures:load
```


### Développement

Lance le serveur php et le serveur d'assets
```
npm run start
```

docker-compose exec cli bash

