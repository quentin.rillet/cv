<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Message;
use App\Form\DocumentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin")
     *
     * @return Response
     */
    public function index(): Response
    {
        $messages = $this->getDoctrine()->getRepository(Message::class)->findBy([], ['createdAt' => 'DESC']);

        $delete_forms = array_map(
            function ($element) {
                return $this->createDeleteForm($element)->createView();
            }, $messages
        );
        // replace this line with your own code!
        return $this->render('admin/message.html.twig', [
            'messages' => $messages,
            'delete_forms' => $delete_forms,
        ]);
    }

    /**
     * @Route("/message/{id}/delete", name="message_delete", methods={"DELETE"})
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(int $id): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository(Message::class)->find($id);
        if ($message) {
            $this->addFlash(
                'notice',
                'Message bien supprimé'
            );
            $em->remove($message);
            $em->flush();
        } else {
            $this->addFlash(
                'notice',
                'Pas de message trouvé'
            );
        }

        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/cv", name="cv")
     *
     * @return Response
     */
    public function cvAction(Request $request): Response
    {
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var UploadedFile
             */
            $file = $document->getNom();
            $fileName = 'CV-Resume-Quentin-Rillet.'.$file->guessExtension();
            $file->move(
                $this->getParameter('documents_directory'),
                $fileName
            );
            $document->setNom($fileName);
            $this->addFlash(
                'notice',
                'Votre CV est en ligne'
            );

            return $this->redirect($this->generateUrl('cv'));
        }

        return $this->render('admin/cv.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/recommendation", name="recommendation")
     *
     * @return Response
     */
    public function recommendation(Request $request): Response
    {
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var UploadedFile
             */
            $file = $document->getNom();
            //$fileName = md5(uniqid()).'.'.$file->guessExtension();
            $fileName = 'recommendation-Quentin-Rillet.'.$file->guessExtension();
            $file->move(
                $this->getParameter('documents_directory'),
                $fileName
            );
            $document->setNom($fileName);
            $this->addFlash(
                'notice',
                'Votre lettre est en ligne'
            );

            return $this->redirect($this->generateUrl('recommendation'));
        }

        return $this->render('admin/recommendation.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a Message entity.
     *
     * @param Message $message
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Message $message): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('message_delete', ['id' => $message->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
